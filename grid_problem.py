import numpy as np
import matplotlib.pyplot as plt

from mdptoolbox.mdp import PolicyIteration, ValueIteration, QLearning
from matplotlib.patches import Rectangle




def build_matrices():
    # N, S, E, W
    actions = [0, 1, 2, 3]

    action_vals = [-4, 4, 1, -1]

    # 4x4 Grid
    states = np.arange(16)


    # This is dimension S, because we only reward based on state and there are S states
    pos_rewards = np.array([0, 0, 0, 1,
                            0, 0, 0, 0,
                            -3, -3, 0, 0,
                            -3, -3, 0, 0])


    # corners = [0, 3, 12, 15]
    top_borders = [0, 1, 2, 3]
    left_borders = [0, 4, 8, 12]
    right_borders = [3, 7, 11, 15]
    bottom_borders = [12, 13, 14, 15]
    # border_inds = [0, 1, 2, 3, 4, 7]

    borders = [top_borders, bottom_borders, right_borders, left_borders]


    transitions = []
    for action in actions:
        transitions.append([])
        # 16 x 16
        for state in states:
            transitions[-1].append([0]*states.shape[0])

            # Handle bumping into walls
            if state in borders[action]:
                transitions[-1][-1][state] = 1
                continue
            else:
                transitions[-1][-1][state+action_vals[action]] = 1

    transitions = np.asarray(transitions)

    return transitions, pos_rewards, states

def policy_iteration(transitions, rewards, discount=0.1, epsilon=None):
    # transitions, rewards, _ = build_matrices()

    pi = PolicyIteration(transitions, rewards, discount, epsilon)

    pi.run()
    print(len(pi.V))

    return pi.policy, pi.iter

def value_iteration(transitions, rewards, discount=0.1, epsilon=None):
    # transitions, rewards, _ = build_matrices()

    vi = ValueIteration(transitions, rewards, discount, epsilon)
    vi.run()

    return vi.policy, vi.iter

def q_learning(transitions, rewards, discount=0.1, epsilon=None):
    # transitions, rewards, _ = build_matrices()

    q = QLearning(transitions, rewards, discount, epsilon)
    q.run()

    return q.policy



def plot_policy(policy, states):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for ind, state in enumerate(reversed(states)):

        ax.text(state%4+.25, ind/4+.25, str(state))
        ax.add_patch(Rectangle((state%4 - .5, ind/4 - .5), 1, 1, facecolor="grey"))

        if policy[state] == 0:
            arrow = (0, 1)
        if policy[state] == 1:
            arrow = (0, -1)
        if policy[state] == 2:
            arrow = (1, 0)
        if policy[state] == 3:
            arrow = (-1, 0)


        ax.arrow(state%4, ind/4, arrow[0]*.1, arrow[1]*.1, head_width=0.05, head_length=0.1, fc='k', ec='k')


    ax.set_xlim(-0.5, 3.5)
    ax.set_ylim(-0.5, 3.5)
    #
    # print(transitions[2])
    plt.show()


if __name__ == "__main__":

    transitions, rewards, states = build_matrices()

    pi_policy = policy_iteration(transitions, rewards)

    plot_policy(pi_policy, states)

