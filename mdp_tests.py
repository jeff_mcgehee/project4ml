__author__ = 'Jeff'

import mdptoolbox.example

# Thermostat MDP
# State Variables: Heat/Cool/Fan/Off, Stove Off/On, 12hr*(outside, sunlight), max des. temp, min des. temp,
#                  target sensor, LR temp, Hall temp, Basement temp, compressor hours this month,
#                  dtemp over past x minutes
#
# Actions: Heat/Cool/Fan/Off
#

P, R = mdptoolbox.example.rand(4,3)

print(P)
print(P.shape)


print(R)
print(R.shape)

