__author__ = 'Jeff'

from therm_model import ThermSim, default_sim

import cPickle as pickle
import time

import matplotlib.pyplot as plt

import numpy as np
import random
from mdptoolbox.mdp import PolicyIteration, ValueIteration, QLearning, PolicyIterationModified




def load_trans_reward(path='/Users/Jeff/BitBucket/School/Python/Project4ML/data/rewards_transitions/fixed_large_cut_on_penalty.npz'):
    data_dict = np.load(path)

    return data_dict['transition'], data_dict['reward']


def plot_default(show_fig=True):
    # np.random.seed(0)
    # random.seed(0)

    sim = default_sim()


    dumb_policy = sim.build_default_policy()

    results = sim.simulate_policy(dumb_policy, n_hours=24*10, detailed_results=True)

    print(results.compressor_hours)
    print(np.sum(results.rewards))

    ax = results.plot_temps()

    ax.legend()

    if show_fig:
        plt.show()

    return ax, results

def plot_pi(show_fig=True):


    transition, reward = load_trans_reward()


    pi = PolicyIterationModified(transition, reward, 0.01, epsilon=0.000001)
    pi.verbose = True
    pi.run()

    therm_policy = [['cool', 'fan', 'off'][val] for val in pi.policy]

    print(therm_policy)

    sim2 = default_sim()

    # np.random.seed(10)
    # random.seed(100)
    results = sim2.simulate_policy(therm_policy, n_hours=24*10,  detailed_results=True)

    print(results.compressor_hours)
    print(np.sum(results.rewards))

    ax = results.plot_temps()
    ax.set_title('Policy Iteration Policy')
    ax.legend()

    if show_fig:
        plt.show()

    return ax, results

def plot_vi(show_fig=True):


    transition, reward = load_trans_reward()


    vi = ValueIteration(transition, reward, 0.01, epsilon=0.00001, max_iter=10000)
    vi.verbose = True
    vi.run()

    therm_policy = [['cool', 'fan', 'off'][val] for val in vi.policy]

    print(therm_policy)

    sim2 = default_sim()

    # np.random.seed(0)
    # random.seed(0)
    results = sim2.simulate_policy(therm_policy, n_hours=24*10,  detailed_results=True)

    print(results.compressor_hours)
    print(np.sum(results.rewards))

    ax = results.plot_temps()
    ax.set_title('Value Iteration Policy')
    ax.legend()

    if show_fig:
        plt.show()

    return ax, results

def plot_q(ql=None, show_fig=True):

    transition, reward = load_trans_reward()

    if not ql:
        print('Initializing Q Learner')
        ql = QLearning(transition, reward, 0.000000001, n_iter=10000)

        ql.setVerbose()
        print('Running Q Learner')
        ql.run()

        therm_policy = [['cool', 'fan', 'off'][val] for val in ql.policy]
    else:
        therm_policy = [['cool', 'fan', 'off'][val] for val in ql['policy']]

    print(therm_policy)

    sim2 = default_sim()

    # np.random.seed(0)
    # random.seed(0)
    results = sim2.simulate_policy(therm_policy, n_hours=24*10,  detailed_results=True)

    print(results.compressor_hours)
    print(np.sum(results.rewards))

    ax = results.plot_temps()
    ax.set_title('Q Learning Policy')
    ax.legend()

    if show_fig:
        plt.show()

    return ax, results


def q_learner_train(input_path):

    transition, reward = load_trans_reward(input_path)

    print('Initializing Q Learner')
    ql = QLearning(transition, reward, 0.9, n_iter=5000000)

    print('Running Q Learner')
    ql.run()

    results_dict = dict(Q=ql.Q, V=ql.V, policy=ql.policy, mean_discrepancy=ql.mean_discrepancy)

    with open('q_learner0_9_bowl_5M', 'wb') as out_file:
        pickle.dump(results_dict, out_file)


def load_q(path):
    with open(path, 'rb') as open_file:
        ql = pickle.load(open_file)

    return ql


if __name__ == "__main__":
    # q_learner_sim('/Users/Jeff/BitBucket/School/Python/Project4ML/data/rewards_transitions/fixed_thermostat_data_simple_bowl.npz')
    plot_default(show_fig=False)
    # plot_pi(show_fig=False)
    # plot_vi(show_fig=False)
    plot_q(load_q('/Users/Jeff/BitBucket/School/Python/Project4ML/q_learner0_9_cut_on_5M'),show_fig=False)
    # plot_q(show_fig=False)
    plt.show()